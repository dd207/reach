import clearSearch from './clearSearch.js';
import sortTables from './sortTables.js';

document.addEventListener('DOMContentLoaded', function(event) {
  clearSearch();
  sortTables();
});
